
#include <iostream>

#define N 4 // size
using namespace std;


int main()
{
    int grid[N][N];
   
    // initialize array
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            grid[i][j] = i + j;
        }
    }
    // print array[N][N]
    cout << "array[" << N <<"][" << N << "]" << endl;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            cout << grid[i][j] << " ";
        }
        cout << endl;
    }
    // print one line 
    cout << "line " << 16 % N << endl;
    for (int j = 0; j < N; j++) {
        cout << grid[16 % N][j] << " ";
    }
}

